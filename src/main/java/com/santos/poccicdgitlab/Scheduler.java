package com.santos.poccicdgitlab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@Component
public class Scheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    //comment
    @Scheduled(cron = "0/10 * 6-17 * * MON-FRI", zone = "America/Sao_Paulo")
    public void scheduleFixedDelayTask() throws InterruptedException {
        LOGGER.info("Fixed delay task - {}", LocalDateTime.now().format(dateTimeFormatter));
        LOGGER.info("Dormindo");
        TimeUnit.SECONDS.sleep(3);
        LOGGER.info("Acordando");
    }
}
