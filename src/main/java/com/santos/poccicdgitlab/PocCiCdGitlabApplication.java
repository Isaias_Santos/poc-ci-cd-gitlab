package com.santos.poccicdgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PocCiCdGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocCiCdGitlabApplication.class, args);
	}

}
